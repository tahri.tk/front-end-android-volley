package com.example.articleapp;

import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;
import com.android.volley.Request;
import com.android.volley.toolbox.JsonObjectRequest;
import org.json.JSONException;
import org.json.JSONObject;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;


public class AddArticleActivity extends AppCompatActivity {

    private EditText txtAuteur, txtContenu, txtTitre;
    private Article article;
    private int requestType = Request.Method.POST;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_article);

        txtAuteur = findViewById(R.id.txt_add_auteur);
        txtContenu = findViewById(R.id.txt_add_contenu);
        txtTitre = findViewById(R.id.txt_add_titre);

        Intent intent = getIntent();
        article = (Article) intent.getSerializableExtra("ARTICLE");

        if (article != null) {
            txtTitre.setText(article.getTitre());
            txtContenu.setText(article.getContenu());
            txtAuteur.setText(article.getAuteur());
            requestType = Request.Method.PUT;
        }
    }

    public void btnAjouterArticle(View view) throws JSONException {

        JSONObject object = new JSONObject();
        if (requestType == Request.Method.PUT) {
            object.put("id", article.getId());
        }
        object.put("titre", txtTitre.getText().toString());
        object.put("auteur", txtAuteur.getText().toString());
        object.put("date", LocalDate.now().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));
        object.put("contenu", txtContenu.getText().toString());

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                requestType,
                "http://192.168.1.109:8080/articles",
                object,
                response -> startActivity(new Intent(getApplicationContext(), MainActivity.class)),
                error -> Toast.makeText(getApplicationContext(), "Erreur de connexion", Toast.LENGTH_LONG));

        VolleyHelper.getInstance(this).addRequest(jsonObjectRequest);
    }
}
