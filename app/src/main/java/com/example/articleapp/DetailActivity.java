package com.example.articleapp;

import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;
import com.android.volley.Request;
import com.android.volley.toolbox.StringRequest;

public class DetailActivity extends AppCompatActivity {

    private TextView txtAuteur, txtContenu, txtTitre, txtDate;
    private Article article;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        txtAuteur = findViewById(R.id.txt_detail_auteur);
        txtContenu = findViewById(R.id.txt_detail_contenu);
        txtDate = findViewById(R.id.txt_detail_date);
        txtTitre = findViewById(R.id.txt_detail_titre);

        Intent intent = getIntent();
        article = (Article) intent.getSerializableExtra("ARTICLE");

        if (article != null) {
            txtTitre.setText(article.getTitre());
            txtContenu.setText(article.getContenu());
            txtDate.setText(article.getDate().toString());
            txtAuteur.setText(article.getAuteur());
        }

    }

    public void btnModifierArticle(View view) {
        Intent intent=new Intent(getApplicationContext(),AddArticleActivity.class);
        intent.putExtra("ARTICLE", article);
        startActivity(intent);
    }

    public void btnSupprimerArticle(View view) {
        StringRequest stringRequest = new StringRequest(
                Request.Method.DELETE,
                "http://192.168.1.109:8080/articles/" + article.getId(),
                null,
                error -> Toast.makeText(getApplicationContext(), "Erreur de connexion", Toast.LENGTH_LONG));
        VolleyHelper.getInstance(this).addRequest(stringRequest);
        startActivity(new Intent(getApplicationContext(), MainActivity.class));
    }
}
