package com.example.articleapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;
import com.android.volley.Request;
import com.android.volley.toolbox.JsonArrayRequest;
import org.json.JSONException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private ListView listView;
    List<Article> articles =new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        listView=findViewById(R.id.listView);

        listView.setOnItemClickListener((parent, view, position, id) -> {
            Intent intent=new Intent(getApplicationContext(),DetailActivity.class);
            intent.putExtra("ARTICLE", articles.get(position));
            startActivity(intent);
        });

        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(
            Request.Method.GET,
            "http://192.168.1.109:8080/articles",
            null,

            response -> {
                for (int i = 0; i < response.length(); i++) {
                    try {
                        Article article=new Article();

                        article.setId(response.getJSONObject(i).getLong("id"));
                        article.setTitre(response.getJSONObject(i).getString("titre"));
                        article.setContenu(response.getJSONObject(i).getString("contenu"));
                        article.setAuteur(response.getJSONObject(i).getString("auteur"));
                        LocalDate date=LocalDate.parse(response.getJSONObject(i).getString("date"), DateTimeFormatter.ofPattern("dd/MM/yyyy"));
                        article.setDate(date);

                        articles.add(article);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                ArticleAdapter articleAdapter=new ArticleAdapter(getApplicationContext(),articles);
                listView.setAdapter(articleAdapter);
            }, null);

        VolleyHelper.getInstance(this).addRequest(jsonArrayRequest);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if(item.getItemId() == R.id.menu_ajouter){
            Intent intent=new Intent(this,AddArticleActivity.class);
            startActivity(intent);
        }
        return true;
    }
}
