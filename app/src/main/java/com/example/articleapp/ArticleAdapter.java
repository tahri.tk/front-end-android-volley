package com.example.articleapp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import java.time.format.DateTimeFormatter;
import java.util.List;

public class ArticleAdapter extends BaseAdapter {
    Context context;
    LayoutInflater layoutInflater;
    List<Article> articles;

    public ArticleAdapter(Context context, List<Article> articles) {
        this.context = context;
        this.articles = articles;
    }

    @Override
    public int getCount() {
        return articles.size();
    }

    @Override
    public Object getItem(int position) {
        return articles.get(position);
    }

    @Override
    public long getItemId(int position) {
        return articles.get(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (layoutInflater == null) {
            layoutInflater= (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        if (convertView == null){
            convertView=layoutInflater.inflate(R.layout.article_item,null);
        }

        TextView titre=convertView.findViewById(R.id.txtTitre);
        TextView contenu=convertView.findViewById(R.id.txtContenu);
        TextView auteur=convertView.findViewById(R.id.txtAuteur);
        TextView date=convertView.findViewById(R.id.txtDate);

        titre.setText(articles.get(position).getTitre());
        contenu.setText(articles.get(position).getContenu());
        auteur.setText(articles.get(position).getAuteur());
        date.setText(articles.get(position).getDate().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));

        return convertView;
    }
}
